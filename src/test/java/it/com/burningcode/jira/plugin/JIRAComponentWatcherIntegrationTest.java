/* Copyright (c) 2008, 2009, Ray Barham
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *     * Redistributions of source code must retain the above copyright
 *       notice, this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the project nor the
 *       names of its contributors may be used to endorse or promote products
 *       derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY Ray Barham ''AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL Ray Barham BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package it.com.burningcode.jira.plugin;

import com.atlassian.integrationtesting.runner.restore.Restore;
import com.atlassian.jira.event.type.EventType;
import com.atlassian.jira.functest.framework.LoginAs;
import com.atlassian.jira.functest.framework.email.OutgoingMailHelper;
import com.atlassian.jira.functest.framework.page.IssueSearchPage;
import com.atlassian.jira.notification.type.NotificationType;
import com.burningcode.jira.plugin.settings.NotificationTypeSetting;
import com.icegreen.greenmail.util.GreenMail;
import com.icegreen.greenmail.util.ServerSetupTest;
import it.com.burningcode.jira.BaseIntegrationTest;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.xml.sax.SAXException;

import javax.inject.Inject;
import javax.mail.Address;
import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.util.*;

import static com.atlassian.jira.functest.framework.FunctTestConstants.*;
import static it.com.burningcode.jira.IntegrationTestHelper.*;
import static org.junit.Assert.*;

/**
 * Integration test for the JIRA Component Watcher Plugin.
 * 
 * TODO: Add test for adding user watchers in the group area and group watchers in the user area
 * 
 * @author Ray Barham
 */
@LoginAs(user = "admin")
public class JIRAComponentWatcherIntegrationTest extends BaseIntegrationTest {
    @Inject
    private OutgoingMailHelper mailHelper;

    private final GreenMail greenMail = new GreenMail(ServerSetupTest.ALL);

    @Before
    public void setup() throws Exception {
        toggleAutoWatch(false);
        toggleEmailUserOnOwnChanges(false);
    }

    @After
    public void teardown() {
        greenMail.stop();
    }

    public void assertRecipientsHaveMessages(final String[] expectedRecipients) throws MessagingException {
        MimeMessage[] messages = greenMail.getReceivedMessages();
        ArrayList<String> recipients = new ArrayList<>();
        for(final MimeMessage message : messages) {
            for(final Address address : message.getAllRecipients()) {
                recipients.add(address.toString());
            }
        }

        for(final String expectedRecipient : expectedRecipients) {
            assertTrue("Did not find '" + expectedRecipient + "' as an email recipient", recipients.contains(expectedRecipient));
        }
    }

    public void assertNeMessagesReceived() {
        MimeMessage[] messages = greenMail.getReceivedMessages();
        assertTrue("Found '" + messages.length + "' messages when there shouldn't have been.", messages.length == 0);
    }

    protected void configureAndStartSmtpServer() {
        greenMail.start();
        backdoor.mailServers().addSmtpServer(greenMail.getSmtp().getPort());
    }

    protected void flushMailQueueAndWait(final int messageCount) throws InterruptedException {
        flushMailQueue();
        greenMail.waitForIncomingEmail(messageCount);
    }

    @Test
    @Restore("WithoutComponentWatchers.zip")
    public void testAccessingComponentWatchersPage() {
        gotoProjectAdminByKey(PROJECT_KEY);
    	tester.assertLinkPresentWithText(COMPONENT_WATCHERS_LINK);
    	navigation.clickLinkWithExactText(COMPONENT_WATCHERS_LINK);

    	assertions.getTextAssertions().assertTextPresent("JIRA Component Watcher - Test");
    }

    @Test
    @Restore("WithoutComponentWatchers.zip")
    public void testAddingComponentWatchers() throws IOException, SAXException {
    	gotoProjectComponentWatchers(PROJECT_KEY);

    	// Test adding users
        gotoEditUserWatchersScreen(COMPONENT_ART_ID);
    	assertions.getTextAssertions().assertTextPresent("Add Users");
    	tester.getDialog().setFormParameter("watcherField", ADMIN_USERNAME);
    	tester.getDialog().submit("add_watchers");

    	assertions.getTextAssertions().assertTextPresent(ADMIN_FULLNAME + " (" + ADMIN_USERNAME + ")");
    	tester.assertFormElementPresent("removeWatcher_" + ADMIN_USERNAME);

    	tester.clickLinkWithText("Component Watchers");
    	tester.assertElementPresent("component_" + COMPONENT_ART_ID + "_user_watcher_" + ADMIN_USERNAME);

    	// Test adding groups
    	tester.clickLink("editGroupWatchers_" + COMPONENT_CODE_ID);
    	assertions.getTextAssertions().assertTextPresent("Add Groups");
    	form.selectOptionsByValue("groupWatchers[]", new String[]{JIRA_DEV_GROUP, JIRA_ADMIN_GROUP});
    	tester.getDialog().submit("add_watchers");

    	assertions.getTextAssertions().assertTextPresent(JIRA_DEV_GROUP);
    	tester.assertFormElementPresent("removeWatcher_" + JIRA_DEV_GROUP);
    	assertions.getTextAssertions().assertTextPresent(JIRA_ADMIN_GROUP);
    	tester.assertFormElementPresent("removeWatcher_" + JIRA_ADMIN_GROUP);
    	tester.clickLinkWithText("Component Watchers");

    	tester.assertElementPresent("component_" + COMPONENT_CODE_ID + "_group_watcher_" + JIRA_DEV_GROUP);
    	tester.assertElementPresent("component_" + COMPONENT_CODE_ID + "_group_watcher_" + JIRA_ADMIN_GROUP);
    }

    @Test
    @Restore("WithComponentWatchers.zip")
    public void testAddingDuplicateComponentWatchers(){
    	// Test adding users with a user already added
        addComponentWatcherUsers(PROJECT_KEY, COMPONENT_ART_ID, ADMIN_USERNAME + "," + BOB_USERNAME);

    	assertions.getTextAssertions().assertTextPresent("\"" + ADMIN_USERNAME + "\" is already a watcher on this component.");

    	// Make sure the other user was adding along side the duplicate user
    	assertions.getTextAssertions().assertTextPresent(ADMIN_FULLNAME + " (" + ADMIN_USERNAME + ")");
    	tester.assertFormElementPresent("removeWatcher_" + ADMIN_USERNAME);
    	assertions.getTextAssertions().assertTextPresent(BOB_FULLNAME + " (" + BOB_USERNAME + ")");
    	tester.assertFormElementPresent("removeWatcher_" + BOB_USERNAME);

    	tester.clickLinkWithText("Component Watchers");

    	tester.assertElementPresent("component_" + COMPONENT_ART_ID + "_user_watcher_" + ADMIN_USERNAME);
    	tester.assertElementPresent("component_" + COMPONENT_ART_ID + "_user_watcher_" + BOB_USERNAME);
    }

    @Test
    @Restore("WithComponentWatchers.zip")
    public void testComponentWatchersPagePermissions() {
    	String user = "jdoe";
    	String expectedMessage = "Access Denied";
    	String componentWatcherPath = "/secure/project/EditComponentWatcher.jspa?projectId="+Integer.toString(PROJECT_ID);
    	String userWatcherPath = "/secure/project/EditComponentUserWatcher.jspa?componentId="+COMPONENT_ART_ID;
    	String groupWatcherPath = "/secure/project/EditComponentGroupWatcher.jspa?componentId="+COMPONENT_ART_ID;

        backdoor.usersAndGroups().addUser(user);
        backdoor.usersAndGroups().addUserToGroup(user, JIRA_USERS_GROUP);

    	navigation.login(user);
    	navigation.gotoPage(componentWatcherPath);
    	assertions.getTextAssertions().assertTextPresent(expectedMessage);
    	navigation.gotoPage(userWatcherPath);
    	assertions.getTextAssertions().assertTextPresent(expectedMessage);
    	navigation.gotoPage(groupWatcherPath);
    	assertions.getTextAssertions().assertTextPresent(expectedMessage);

    	navigation.login(ADMIN_USERNAME);
    	backdoor.usersAndGroups().addUserToGroup(user, JIRA_ADMIN_GROUP);

    	navigation.login(user);
    	navigation.gotoPage(componentWatcherPath);
    	assertions.getTextAssertions().assertTextNotPresent(expectedMessage);
    	navigation.gotoPage(userWatcherPath);
    	assertions.getTextAssertions().assertTextNotPresent(expectedMessage);
    	navigation.gotoPage(groupWatcherPath);
    	assertions.getTextAssertions().assertTextNotPresent(expectedMessage);
    }

    @Test
    @Restore("WithComponentWatchers.zip")
    public void testComponentUserWatchersToGroupLink(){
        gotoProjectAdminByKey(PROJECT_KEY);
    	navigation.clickLinkWithExactText(COMPONENT_WATCHERS_LINK);
        gotoEditUserWatchersScreen(COMPONENT_ART_ID);
    	tester.clickLinkWithText("Edit Group Watchers");

    	assertions.getURLAssertions().assertCurrentURLEndsWith("EditComponentGroupWatcher.jspa?componentId=" + COMPONENT_ART_ID);
    }

    @Test
    @Restore("WithoutComponentWatchers.zip")
    public void testEnableComponentWatchers(){
        assertEquals(backdoor.plugins().getPluginState(PLUGIN_KEY), "ENABLED");
        backdoor.plugins().disablePlugin(PLUGIN_KEY);
        assertEquals(backdoor.plugins().getPluginState(PLUGIN_KEY), "DISABLED");
    }

    @Test
    @Restore("WithComponentWatchers.zip")
    public void testNotificationsWithUserWatchers() throws InterruptedException, MessagingException, IOException {
        configureAndStartSmtpServer();

    	/* TODO: Enable when custom notification types can be handled by JIRA again.
    	addNotificationToScheme(EventType.ISSUE_CREATED_ID, ComponentWatcherNotificationType.getLabel());
    	*/

        backdoor.usersAndGroups().addUser(FRED_USERNAME);

//    	// Made user a component watcher
        addComponentWatcherUsers(PROJECT_KEY, COMPONENT_OTHER_ID, BOB_USERNAME + "," + FRED_USERNAME + "," + ADMIN_USERNAME);

    	// Create issue that should be received by two users
    	String subject = "Test issue with user watcher";
    	Map<String, String[]> params = new HashMap<>();
    	params.put("components", new String[]{String.valueOf(COMPONENT_OTHER_ID)});
    	String issue = navigation.issue().createIssue(PROJECT_NAME, ISSUE_TYPE_BUG, subject, params);
    	navigation.issue().gotoIssue(issue);

        flushMailQueueAndWait(2);
    	assertRecipientsHaveMessages(new String[]{BOB_EMAIL, FRED_EMAIL});
    }

    @Test
    @Restore("WithComponentWatchers.zip")
    public void testNotificationsWithGroupWatchers() throws InterruptedException, MessagingException, IOException {
        configureAndStartSmtpServer();

    	/* TODO: Enable when custom notification types can be handled by JIRA again.
    	addNotificationToScheme(EventType.ISSUE_CREATED_ID, ComponentWatcherNotificationType.getLabel());
    	*/

        backdoor.usersAndGroups().addUser(FRED_USERNAME);
        backdoor.usersAndGroups().addUserToGroup(BOB_USERNAME, JIRA_DEV_GROUP);
        backdoor.usersAndGroups().addUserToGroup(FRED_USERNAME, JIRA_DEV_GROUP);

    	// Made user a component watcher
    	gotoProjectComponentWatchers(PROJECT_KEY);
    	tester.clickLink("editGroupWatchers_" + COMPONENT_OTHER_ID);
    	tester.selectOption("groupWatchers[]", JIRA_DEV_GROUP);
    	tester.submit("add_watchers");
    	//tester.getDialog().submit("add_watchers");

    	// Create issue that should be received by two users
    	String subject = "Test issue with group watchers";
    	Map<String, String[]> params = new HashMap<>();
    	params.put("components", new String[]{String.valueOf(COMPONENT_OTHER_ID)});
    	String issue = navigation.issue().createIssue(PROJECT_NAME, ISSUE_TYPE_BUG, subject, params);
    	navigation.issue().gotoIssue(issue);

        flushMailQueueAndWait(2);
        assertRecipientsHaveMessages(new String[]{BOB_EMAIL, FRED_EMAIL});
    }

    @Test
    @Restore("WithComponentWatchers.zip")
    public void testDuplicateNotifications() throws InterruptedException, MessagingException, IOException, SAXException {
        configureAndStartSmtpServer();

    	/* TODO: Enable when custom notification types can be handled by JIRA again.
    	addNotificationToScheme(EventType.ISSUE_CREATED_ID, ComponentWatcherNotificationType.getLabel());
    	*/

        backdoor.usersAndGroups().addUser(FRED_USERNAME);
        backdoor.usersAndGroups().addUserToGroup(BOB_USERNAME, JIRA_DEV_GROUP);
        backdoor.usersAndGroups().addUserToGroup(FRED_USERNAME, JIRA_DEV_GROUP);
        backdoor.usersAndGroups().addUserToGroup(FRED_USERNAME, JIRA_ADMIN_GROUP);

        // Made user a component watcher
        addComponentWatcherUsers(PROJECT_KEY, COMPONENT_CODE_ID, FRED_USERNAME + "," + BOB_USERNAME);

        gotoProjectComponentWatchers(PROJECT_KEY);
        tester.clickLink("editGroupWatchers_" + COMPONENT_OTHER_ID);
        tester.selectOption("groupWatchers[]", JIRA_DEV_GROUP);
        tester.submit("add_watchers");

        // Create issue that should be received by two users
        String subject = "Test issue with group watchers";
        Map<String, String[]> params = new HashMap<>();
        params.put("components", new String[]{String.valueOf(COMPONENT_CODE_ID), String.valueOf(COMPONENT_OTHER_ID)});
        String issue = navigation.issue().createIssue(PROJECT_NAME, ISSUE_TYPE_BUG, subject, params);
        navigation.issue().gotoIssue(issue);

        flushMailQueueAndWait(2);
        assertRecipientsHaveMessages(new String[]{BOB_EMAIL, FRED_EMAIL});
    }

    @Test
    @Restore("WithComponentWatchers.zip")
    public void testNotificationsWithEventUserGettingNotification() throws InterruptedException, MessagingException, IOException {
        configureAndStartSmtpServer();

        toggleEmailUserOnOwnChanges(true);

    	/* TODO: Enable when custom notification types can be handled by JIRA again.
    	addNotificationToScheme(EventType.ISSUE_CREATED_ID, ComponentWatcherNotificationType.getLabel());
    	*/

        backdoor.usersAndGroups().addUser(FRED_USERNAME);

        // Made user a component watcher
        addComponentWatcherUsers(PROJECT_KEY, COMPONENT_OTHER_ID, BOB_USERNAME + "," + FRED_USERNAME + "," + ADMIN_USERNAME);

        // Create issue that should be received by two users
        String subject = "Test issue with user watcher";
        Map<String, String[]> params = new HashMap<>();
        params.put("components", new String[]{String.valueOf(COMPONENT_OTHER_ID)});
        String issue = navigation.issue().createIssue(PROJECT_NAME, ISSUE_TYPE_BUG, subject, params);
        navigation.issue().gotoIssue(issue);

        flushMailQueueAndWait(3);
        assertRecipientsHaveMessages(new String[]{BOB_EMAIL, FRED_EMAIL, ADMIN_EMAIL});
    }

    /**
     * Test removing all users and groups using the "All" checkbox
     *
     * TODO: Uses javascript to select all watcher.  Find a way to do integration test w/ javascript.
     */
    /*
    @Test
    public void testRemoveAllCheckbox() {
    	testAddingComponentWatchers();
    	WebLink[] links = page.getLinksWithExactText("Edit");
    	log.log("Found "+links.length+" 'Edit' links.");

    	for(WebLink link : links){
    		log.log("Clicking link: "+link.asText());
    		navigation.clickLink(link);
    		tester.checkCheckbox("removeAllWatchers");
    		tester.getDialog().submit("remove_watchers");

    		if(link.getID().startsWith("editUserWatchers_")){
    			log.log("editUserWatchers");
        		assertFalse("Admin still found after deletion", tester.getDialog().hasFormParameterNamed("removeWatcher_"+ADMIN));
        		assertFalse("Bob still found after deletion", tester.getDialog().hasFormParameterNamed("removeWatcher_"+USER_BOB));
    		}else if(link.getID().startsWith("editGroupWatchers_")){
    			log.log("editGroupWatchers");
    			assertFalse("jira-users still found after deletion", tester.getDialog().hasFormParameterNamed("removeWatcher_jira-users"));
    			assertFalse("jira-developers still found after deletion", tester.getDialog().hasFormParameterNamed("removeWatcher_jira-developers"));
    		}

    		tester.clickLink("return_link");
    	}
    }*/

    @Test
    @Restore("WithComponentWatchers.zip")
    public void testRemovingComponentWatchers() throws Exception {
        removeComponentWatcherUser(PROJECT_KEY, COMPONENT_ART_ID, ADMIN_USERNAME);

    	tester.clickLinkWithText("Component Watchers");
    	tester.assertElementNotPresent("component_" + COMPONENT_ART_ID + "_user_watcher_" + ADMIN_USERNAME);

        removeComponentWatcherGroup(PROJECT_KEY, COMPONENT_CODE_ID, JIRA_ADMIN_GROUP);

    	tester.clickLinkWithText("Component Watchers");

    	tester.assertElementPresent("component_" + COMPONENT_CODE_ID + "_group_watcher_" + JIRA_DEV_GROUP);
    	tester.assertElementNotPresent("component_" + COMPONENT_CODE_ID + "_group_watcher_" + JIRA_ADMIN_GROUP);
    }

    @Test
    @Restore("WithComponentWatchers.zip")
    public void testRemovingWatcherAlreadyDeleted() {
        gotoProjectComponentWatchers(PROJECT_KEY);
        addComponentWatcherUsers(COMPONENT_ART_ID, BOB_USERNAME);

        assertions.getTextAssertions().assertTextPresent(BOB_FULLNAME + " (" + BOB_USERNAME + ")");

        backdoor.usersAndGroups().deleteUser(BOB_USERNAME);

        gotoProjectComponentWatchers(PROJECT_KEY);

        assertions.getTextAssertions().assertTextNotPresent(BOB_FULLNAME);
        assertions.getTextAssertions().assertTextNotPresent(BOB_USERNAME);

        gotoEditUserWatchersScreen(COMPONENT_ART_ID);

        assertions.getTextAssertions().assertTextNotPresent(BOB_FULLNAME);
        assertions.getTextAssertions().assertTextNotPresent(BOB_USERNAME);
    }

    @Test
    @Restore("WithComponentWatchers.zip")
    public void testComponentWatcherCustomFieldSearching() {
        createCustomField("Component Watchers");

        // Made user a component watcher
        addComponentWatcherUsers(PROJECT_KEY, COMPONENT_OTHER_ID, BOB_USERNAME);

        // Create issue that should be received by two users
        String subject = "Test searching component watchers";
        Map<String, String[]> params = new HashMap<>();
        params.put("components", new String[]{String.valueOf(COMPONENT_OTHER_ID)});
        String issue = navigation.issue().createIssue(PROJECT_NAME, ISSUE_TYPE_BUG, subject, params);
        navigation.issue().gotoIssue(issue);

        backdoor.indexing().reindexAll();

        List<String> expectedIssueKeys = Arrays.asList(issue);
        IssueSearchPage search = navigation.issueNavigator().runPrintableSearch("\"Component Watchers\" = bob");
        assertTrue(search.hasResultsTable());
        assertEquals("Did not find user in search results", expectedIssueKeys, search.getResultsIssueKeys());

        search = navigation.issueNavigator().runPrintableSearch("\"Component Watchers\" = admin");
        assertFalse("Found user in search results when shouldn't", search.hasResultsTable());
    }

    @Test
    @Restore("WithComponentWatchers.zip")
    public void testMissingFieldCausesReindexMessageNotToDisplay() {
        backdoor.indexing().clearPendingReindexRequests();
        gotoProjectComponentWatchers(PROJECT_KEY);
        addComponentWatcherUsers(COMPONENT_OTHER_ID, BOB_USERNAME);

        assertions.getTextAssertions().assertTextNotPresent("com.atlassian.jira.reindex.required");
    }

    @Test
    @Restore("WithComponentWatchers.zip")
    public void testFieldCausesReindexMessageToDisplay() {
        backdoor.indexing().clearPendingReindexRequests();
        createCustomField("Component Watchers");
        gotoProjectComponentWatchers(PROJECT_KEY);
        addComponentWatcherUsers(COMPONENT_OTHER_ID, BOB_USERNAME);

        assertions.getTextAssertions().assertTextPresent("com.atlassian.jira.reindex.required");
    }

    @Test
    @Restore("WithComponentWatchers.zip")
    public void testUsernameChangeDisplaysUsersCorrectly() {
        addComponentWatcherUsers(PROJECT_KEY, COMPONENT_ART_ID, BOB_USERNAME);

        assertions.getTextAssertions().assertTextPresent(BOB_FULLNAME + " (" + BOB_USERNAME + ")");
        tester.assertFormElementPresent("removeWatcher_" + BOB_USERNAME);

        tester.clickLinkWithText("Component Watchers");
        tester.assertElementPresent("component_" + COMPONENT_ART_ID + "_user_watcher_" + BOB_USERNAME);

        String newUsername = "newbob";
        editUsername(BOB_USERNAME, newUsername);

        gotoProjectComponentWatchers(PROJECT_KEY);

        tester.clickLinkWithText("Component Watchers");
        tester.assertElementPresent("component_" + COMPONENT_ART_ID + "_user_watcher_" + newUsername);

        gotoEditUserWatchersScreen(COMPONENT_ART_ID);

        assertions.getTextAssertions().assertTextPresent(BOB_FULLNAME + " (" + newUsername + ")");
        tester.assertFormElementPresent("removeWatcher_" + newUsername);
    }

    @Test
    @Restore("WithComponentWatchers.zip")
    public void testUsernameChangeAddsUsersCorrectly() {
        String newUsername = "newbob";
        editUsername(BOB_USERNAME, newUsername);

        addComponentWatcherUsers(PROJECT_KEY, COMPONENT_ART_ID, newUsername);

        assertions.getTextAssertions().assertTextPresent(BOB_FULLNAME + " (" + newUsername + ")");
        tester.assertFormElementPresent("removeWatcher_" + newUsername);

        tester.clickLinkWithText("Component Watchers");
        tester.assertElementPresent("component_" + COMPONENT_ART_ID + "_user_watcher_" + newUsername);
    }

    @Test
    @Restore("WithComponentWatchers.zip")
    public void testUsernameChangeRemovesUsersCorrectly() throws Exception {
        addComponentWatcherUsers(PROJECT_KEY, COMPONENT_ART_ID, BOB_USERNAME);

        String newUsername = "newbob";
        editUsername(BOB_USERNAME, newUsername);

        removeComponentWatcherUser(PROJECT_KEY, COMPONENT_ART_ID, newUsername);

        gotoProjectComponentWatchers(PROJECT_KEY);

        tester.assertElementNotPresent("component_" + COMPONENT_ART_ID + "_user_watcher_" + BOB_USERNAME);
        tester.assertElementNotPresent("component_" + COMPONENT_ART_ID + "_user_watcher_" + newUsername);

        gotoEditUserWatchersScreen(COMPONENT_ART_ID);

        assertions.getTextAssertions().assertTextNotPresent(BOB_FULLNAME + " (" + BOB_USERNAME + ")");
        assertions.getTextAssertions().assertTextNotPresent(BOB_FULLNAME + " (" + newUsername + ")");
        tester.assertFormElementNotPresent("removeWatcher_" + BOB_USERNAME);
        tester.assertFormElementNotPresent("removeWatcher_" + newUsername);
    }

    @Test
    @Restore("WithComponentWatchers.zip")
    public void testWithDifferentNotificationType() throws Exception {
        configureAndStartSmtpServer();

        final String notificationType = NotificationType.COMPONENT_LEAD.dbCode();
        addNotificationToScheme(EventType.ISSUE_CREATED_ID, notificationType);
        setComponentWatcherNotificationType(notificationType);

        backdoor.usersAndGroups().addUser(FRED_USERNAME);

    	// Made user a component watcher
        addComponentWatcherUsers(PROJECT_KEY, COMPONENT_OTHER_ID, BOB_USERNAME + "," + FRED_USERNAME + "," + ADMIN_USERNAME);

        // Create issue that should be received by two users
        String subject = "Test issue with user watcher";
        Map<String, String[]> params = new HashMap<>();
        params.put("components", new String[]{String.valueOf(COMPONENT_OTHER_ID)});
        String issue = navigation.issue().createIssue(PROJECT_NAME, ISSUE_TYPE_BUG, subject, params);
        navigation.issue().gotoIssue(issue);

        flushMailQueueAndWait(2);
        assertRecipientsHaveMessages(new String[]{BOB_EMAIL, FRED_EMAIL});
    }

    @Test
    @Restore("WithComponentWatchers.zip")
    public void testWithNoNotificationType() throws Exception {
        configureAndStartSmtpServer();

        setComponentWatcherNotificationType(NotificationTypeSetting.NO_NOTIFICATION_CODE);

        backdoor.usersAndGroups().addUser(FRED_USERNAME);

        // Made user a component watcher
        addComponentWatcherUsers(PROJECT_KEY, COMPONENT_OTHER_ID, BOB_USERNAME + "," + FRED_USERNAME + "," + ADMIN_USERNAME);

        // Create issue that should be received by two users
        String subject = "Test issue with user watcher";
        Map<String, String[]> params = new HashMap<>();
        params.put("components", new String[]{String.valueOf(COMPONENT_OTHER_ID)});
        String issue = navigation.issue().createIssue(PROJECT_NAME, ISSUE_TYPE_BUG, subject, params);
        navigation.issue().gotoIssue(issue);

        flushMailQueue();
        greenMail.waitForIncomingEmail(2);
        assertNeMessagesReceived();
    }
}