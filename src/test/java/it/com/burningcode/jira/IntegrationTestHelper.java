package it.com.burningcode.jira;

import net.sourceforge.jwebunit.WebTester;

public class IntegrationTestHelper {
	public static final String PLUGIN_KEY = "com.burningcode.jira.plugin.jiracomponentwatcher";
    public static final String FIELD_TYPE = PLUGIN_KEY + ":component-watchers-field-type";
    public static final String FIELD_SEARCH_TYPE = PLUGIN_KEY + ":component-watchers-field-searchers";
    
    public static final String PROJECT_KEY = "TST";
    public static final String PROJECT_NAME = "Test";
    public static final int PROJECT_ID = 10000;

    public static final int COMPONENT_CODE_ID = 10000;
    public static final int COMPONENT_ART_ID = 10001;
    public static final int COMPONENT_OTHER_ID = 10003;
    
    public static final String COMPONENT_WATCHERS_LINK = "Component Watchers";
}
