package com.burningcode.jira.plugin;

import com.atlassian.jira.component.ComponentAccessor;
import com.atlassian.jira.notification.NotificationTypeManager;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.burningcode.jira.plugin.settings.NotificationTypeSetting;
import com.opensymphony.module.propertyset.InvalidPropertyTypeException;
import com.opensymphony.module.propertyset.PropertySet;
import com.opensymphony.module.propertyset.PropertySetManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import webwork.action.ActionContext;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Scanned
public class ComponentWatcherSettings extends JiraWebActionSupport {
	private static final Logger log = LoggerFactory.getLogger(ComponentWatcherSettings.class);
	public static final String notificationType = "notificationType";

	private static PropertySet propertySet;

	@ComponentImport
	private final JiraAuthenticationContext authenticationContext;

	@ComponentImport
	private final GlobalPermissionManager globalPermissionManager;

	private final NotificationTypeManager notificationTypeManager;

	@Inject
	public ComponentWatcherSettings(
			GlobalPermissionManager globalPermissionManager,
			JiraAuthenticationContext authenticationContext) {
		this.authenticationContext = authenticationContext;
		this.globalPermissionManager = globalPermissionManager;
		this.notificationTypeManager = ComponentAccessor.getComponent(NotificationTypeManager.class);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String doDefault() throws Exception {
		if(!hasAdminPermission())
			return PERMISSION_VIOLATION_RESULT;
		
		return super.doDefault();
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	protected String doExecute() throws Exception {
		if(!hasAdminPermission())
			return PERMISSION_VIOLATION_RESULT;
		
		return super.doExecute();
	}

	/**
	 * Called when editing the settings
	 */
	public String doEdit() {
		if(!hasAdminPermission())
			return PERMISSION_VIOLATION_RESULT;
		
		PropertySet propertySet = getProperties();

		Map<?, ?> params = ActionContext.getParameters();

		setStringSetting(propertySet, params, notificationType);

		return getRedirect("ComponentWatcherSettings.jspa");
	}

	public void setStringSetting(PropertySet propertySet, final Map<?, ?> params, final String setting) {
		if(params.containsKey(setting) && propertySet.isSettable(setting)) {
			Object paramValue = params.get(setting);
			if(paramValue instanceof String[] && ((String[])paramValue).length == 1) {
				propertySet.setString(setting, ((String[])paramValue)[0]);
			}
		}
	}

	/**
	 * Static method that returns the PropertySet used to get/store settings in the database
	 * @return The PropertySet to reference the data
	 */
	public static PropertySet getPropertySet() {
		propertySet = null;

		if(propertySet == null) {
			HashMap<String, Object> args = new HashMap<>();
	        args.put("delegator.name", "default");
	        args.put("entityName", "ComponentWatcherSettings");
	        args.put("entityId", (long) 1);

	        propertySet = PropertySetManager.getInstance("ofbiz", args);

			initStringSetting(notificationType, NotificationTypeSetting.defaultCode());
		}

		return propertySet;
	}

	/**
	 * Method used to reference the {@link ComponentWatcherSettings#getPropertySet()}
	 */
    private PropertySet getProperties() {
		return ComponentWatcherSettings.getPropertySet();
	}

	public NotificationTypeSetting getNotificationType() {
		String typeValue = getPropertySet().getString(notificationType);
		NotificationTypeSetting setting = NotificationTypeSetting.from(typeValue, notificationTypeManager);

		if(setting == null) {
			setting = NotificationTypeSetting.defaultSetting(notificationTypeManager);
		}

		return setting;
	}

	public ArrayList<NotificationTypeSetting> getNotificationTypes() {
		return NotificationTypeSetting.all(notificationTypeManager);
	}

	public ApplicationUser getLoggedInUser() {
		return authenticationContext.getLoggedInUser();
	}
	
	/**
	 * Does the current logged in user has admin permissions
	 * @return True if has permissions, false otherwise.
	 */
    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
	private boolean hasAdminPermission() {
        return globalPermissionManager.hasPermission(GlobalPermissionKey.ADMINISTER, getLoggedInUser());
    }

	protected static void initStringSetting(final String setting, final String defaultValue ) {
		try{
			// Set default settings
			if(!propertySet.exists(setting)) {
				propertySet.setString(setting, defaultValue);
			}else{
				// Will throw an exception if of invalid type
				propertySet.getString(setting);
			}
		}catch (InvalidPropertyTypeException e) {
			log.debug("Property '" + setting + "' set to an invalid type.  Setting to default value, " + defaultValue + ".");
			propertySet.setString(setting, defaultValue);
		}catch (Exception e) {
			log.debug("Error while setting String setting '" + setting + "'.", e);
			propertySet.setString(setting, defaultValue);
		}
	}

	public static String getNotificationTypeCode() {
		String code = getPropertySet().getString(notificationType);
		if(code.equals(NotificationTypeSetting.NO_NOTIFICATION_CODE)) {
			return null;
		}
		return code;
	}
}
